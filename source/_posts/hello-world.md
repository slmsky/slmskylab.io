---
title: 博客的主要内容
layout: post
tags: [note]
comments: true
categories: [note]
keywords: hello world
description: Hello World，Dawn，我的博客
photos: 
  - https://raw.githubusercontent.com/slmsky/slmsky.github.io/master/gallery/salt-lake.jpg
---
欢迎来到我的博客，这是我的第一篇博客，博客地址为[https://slmsky.gitlab.io](https://slmsky.gitlab.io)

## 博客的主要内容
1. 随心所想
2. 技术笔记
3. 技术分享，成果展示
4. 感想评论




**2018.7.21 于北京**